var config = {
    map: {
        "*" : {
            'swissclinic/stripe': "Swissclinic_Stripe/js/stripe",
        }
    },
    shim: {
        'stripe': ['jquery']
    }
}