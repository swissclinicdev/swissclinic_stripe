<?php

namespace Swissclinic\Stripe\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected const XML_PATH_SWISSCLINIC_STRIPE_CONFIG= 'swissclinic_stripe/stripe_config/enable';
    protected const XML_PATH_SWISSCLINIC_STRIPE_TEXT= 'swissclinic_stripe/stripe_config/stripe_text';
    protected const XML_PATH_SWISSCLINIC_STRIPE_URL= 'swissclinic_stripe/stripe_config/stripe_url';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_SWISSCLINIC_STRIPE_CONFIG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getText($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_STRIPE_TEXT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     * @return string
     */
    public function getUrlConfig($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_STRIPE_URL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}