<?php
	
	namespace Swissclinic\Stripe\Block;

	class Stripe extends \Magento\Framework\View\Element\Template {
	    /**
	     * @var Swissclinic\PieDisplay\Helper\Data
	     */
	    protected $_shlp;

	    /**
	     * Stripe constructor.
	     * @param \Magento\Catalog\Block\Product\Context $context
	     * @param array $data
	     * @param \Swissclinic\Stripe\Helper\Data $shlp
	     */
	    public function __construct(
	        \Magento\Framework\View\Element\Template\Context $context,
	        array $data = [],
	        \Swissclinic\Stripe\Helper\Data $shlp
	    ){

	        $this->_shlp = $shlp;

	        parent::__construct(
            $context,
            $data);
	    }

	    public function getText() {

	    	$text = $this->_shlp->getText();

	    	return $text;
	    }

	    public function getUrlConfig() {

	    	$text = $this->_shlp->getUrlConfig();

	    	return $text;
	    }
	}